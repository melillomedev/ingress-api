# Ingress API Module

This module provide you some function to fetch data from http://ingress.com/intel.
This module provide autologin functionality with phantomjs

## Installation

``` bash
$ npm i -S git+ssh://git@bitbucket.org:melillomedev/ingress-api.git
```

## Usage

Simple usage

```js
var ingress_api_opt = {
    db: '/opt/ingress/database.sqlite',
    workdir: '/opt/ingress/'
}
var ingress = require('ingress-api')(ingress_api_opt);
```

Use it with socks proxy (tor proxy)

```js
var ingress_api_opt = {
    db: '/opt/ingress/database.sqlite',
    proxy: 'localhost:9050',
    proxytype: 'socks5',
    workdir: '/opt/ingress/'
}
var ingress = require('ingress-api')(ingress_api_opt);
```

Use it with http proxy (squid)

```js
var ingress_api_opt = {
    db: '/opt/ingress/database.sqlite',
    proxy: 'http://localhost:3128',
    proxytype: 'http',
    workdir: '/opt/ingress/'
}
var ingress = require('ingress-api')(ingress_api_opt);
```

## All available paramiters

**db :** Database location

**proxy :** proxy to be userd

**proxytype :** type of proxy (http/socks5)

**workdir :** workdir where config/login.json is located

**phantomjs :** Set location of pahnthomjs binary

**debug :** print debug messages on console

## Change Log

### 0.2.3
  - First released version with proxy support