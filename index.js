var fs = require("fs");

var spawn = require('child_process').spawn
var request = require('request');

var sqlite3 = require("sqlite3").verbose();

module.exports = function(config) {
	var version = null;
	var score = '';
	var display_message = '';
	var child = null;

	var doIntelLogin = require('./lib/phantom').doIntelLogin;

	if (typeof config.debug == 'undefined') {
		config.debug = false;
	}

	var print_console = function(message) {
		if(config.debug) {
			console.log(message);
		}
	}

	if (typeof config.workdir == 'undefined') {
		config.workdir = '/opt/ingress-api-js'
	}

	if (typeof config.db == 'undefined'){
		config.db = config.workdir + '/usage.db'
	}

	var exists = fs.existsSync(config.db);
	if(!exists) {
	    print_console("Creating DB file.");
	    fs.openSync(config.db, "w");
	}
	var db = new sqlite3.Database(config.db);

	db.serialize(function() {
	    if(!exists) {
	        db.run("CREATE TABLE usage (time INTEGER, action TEXT, message TEXT)");
	        db.run('CREATE TABLE regions (regionName TEXT, regionVertices TEXT, timeToEndOfBaseCycleMs INTEGER, updatetime INTEGER, scores TEXT)');
	        db.run("CREATE TABLE login (time INTEGER, csrftoken TEXT, SACSID TEXT)");
	    }
	});

	/**
	 * Save logon data int sqlite database to reuse it later
	 * 
	 * @param  {SACSID[string]}
	 * @param  {csrftoken[string]}
	 */
	var save_login_data = function(SACSID, csrftoken) {
	    var stmt = db.prepare("INSERT INTO login VALUES (?,?,?)");
	    stmt.run(new Date(), csrftoken, SACSID);
	    stmt.finalize();
	}

	var get_login_data = function(callback) {
	    var first = true;
	    var sconf = {
	        csrftoken: '',
	        SACSID: ''
	    };
	    db.serialize(function(){
	        db.each("SELECT * FROM login ORDER BY time DESC LIMIT 1", function(err, row){
	            if(first){
	            	print_console(row);
	                sconf = {
	                    SACSID: row.SACSID,
	                    csrftoken: row.csrftoken
	                };
	                if(typeof row.csrftoken == 'undefined') {
	                    sconf.csrftoken = 'null';
	                }
	                first = false;
	            }
	        }, function(){
	            callback(sconf)
	        });
	    });
	}

	var number_format = function (number, decimals, dec_point, thousands_sep) {
	    var n = !isFinite(+number) ? 0 : +number,
	        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	        toFixedFix = function (n, prec) {
	            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
	            var k = Math.pow(10, prec);
	            return Math.round(n * k) / k;
	        },
	        s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
	    if (s[0].length > 3) {
	        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	    }
	    if ((s[1] || '').length < prec) {
	        s[1] = s[1] || '';
	        s[1] += new Array(prec - s[1].length + 1).join('0');
	    }
	    return s.join(dec);
	};

	var get_regional_scores = function(version, lat, lng, callback) {
	    get_login_data(function(lconfig){
	        print_console(config.SACSID);
	        var options = {
	            url: 'https://www.ingress.com/r/getRegionScoreDetails',
	            method: 'POST',
	            json: true,
	            headers: {
	                "content-type": "application/json",
	                origin: 'https://ingress.com',
	                'x-csrftoken': lconfig.csrftoken,
	                'referer': 'https://www.ingress.com/intel',
	                'authority': 'www.ingress.com',
	                'cookie': "SACSID=" + lconfig.SACSID + '; csrftoken=' + lconfig.csrftoken + ';',
	                'content-type': 'application/json; charset=UTF-8'
	            },
	            body: {
	                latE6: lat,
	                lngE6: lng,
	                v: version
	            }
	        };
	        print_console(JSON.stringify(options));
	        function rcallback(error, response, score) {
	            if (!error && response.statusCode == 200) {
	                var checks = score.result.scoreHistory.length;
	                var enl_total_score = 0;
	                var res_total_score = 0;
	                for(var id = 0 ; id < score.result.scoreHistory.length; id++) {
	                    enl_total_score += parseInt(score.result.scoreHistory[id][1]);
	                    res_total_score += parseInt(score.result.scoreHistory[id][2]);
	                }
	                var enl_total = enl_total_score/checks;
	                var res_total = res_total_score/checks;
	                display_message += 'Region : ' + score.result.regionName + '\n';
	                display_message += 'Scores : ' + checks + ' [R: ' + number_format(res_total)+ ' - E: ' + number_format(enl_total) + ']\n';
	                display_message += 'RES total : ' + number_format(res_total_score) + '\n';
	                display_message += 'ENL total : ' + number_format(enl_total_score) + '\n';
	                display_message += 'Distance R2E : ' + number_format(res_total_score-enl_total_score) + '\n';
	                callback(display_message, score.result.regionName, score.result.regionVertices, score.result.timeToEndOfBaseCycleMs, Date.now());
	                display_message = '';
	                score = '';
	            }
	        }

	        request(options, rcallback);

	    });
	};

	var getIntelVersion = function(callback) {
		function get_intel_version(lconfig) {
			var options = {
	            url: 'https://www.ingress.com/intel',
	            headers: {
	                origin: 'https://ingress.com',
	                'x-csrftoken': lconfig.csrftoken,
	                'referer': 'https://www.ingress.com/intel',
	                'authority': 'www.ingress.com',
	                'cookie': "SACSID=" + lconfig.SACSID + '; csrftoken=' + lconfig.csrftoken + ';'
	            }
	        };
	        function rcallback(error, response, body) {
				if (!error && response.statusCode == 200) {
					var m = body.match(/.*gen_dashboard_(.*).js.*/);
					if (m != null) {
						callback(m[1]);
					} else {
						callback(null);
					}
				} else {
					callback(null);
				}
			}
			request(options, rcallback);
		}
		get_login_data(get_intel_version);
	}

	var start_processing_new = function(lat, lng, callback) {
		getIntelVersion(function(version){
			console.log('version received :' + version);
			if(version === null) {
				display_message = '';
				function loginCredentialsCallback(login) {
					if(typeof login.SACSID == 'undefined' || typeof login.csrftoken == 'undefined'){
						callback('Error retrieving data from intel, please try again later.');
					} else {
						save_login_data(login.SACSID, login.csrftoken);
						//start_processing_new(lat, lng, callback);
						callback('Error retrieving data from intel, please try again later.');
					}
				}
				doIntelLogin(config.username, config.password, config, loginCredentialsCallback)
			} else {
				get_regional_scores(version, lat, lng, callback);
			}
		});
	}

	return {
		testLogin: get_login_data,
		save_login_data: save_login_data,
		getScores: start_processing_new,
		getIntelVersion: getIntelVersion,
		doIntelLogin: doIntelLogin,
		getCycle: require('./lib/tools').getCycle,
	}
}
