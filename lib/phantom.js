var tmp = require('tmp')
var spawn = require('child_process').spawn
var swig  = require('swig');
var fs = require('fs');
var async = require('async');
var shortid = require('shortid');

var template = `
/**
 * Created by gabriel on 07/09/15.
 */

var login_url = 'https://accounts.google.com/ServiceLogin?service=ah&passive=true&continue=https%3A%2F%2Fappengine.google.com%2F_ah%2Fconflogin%3Fcontinue%3Dhttps%3A%2F%2Fwww.ingress.com%2Fintel&ltmpl=gm&shdf=ChMLEgZhaG5hbWUaB0luZ3Jlc3MMEgJhaCIUDxXHTvPWkR39qgc9Ntp6RlMnsagoATIUG3HUffbxSU31LjICBdNoinuaikg';
var fs = require('fs');
var login_config = {
    username: '{{ username }}',
    password: '{{ password }}'
}

var page = require('webpage').create();
page.settings.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36';
page.viewportSize = { width: 1024, height: 768 };
page.clipRect = { top: 0, left: 0, width: 1024, height: 768 };

page.onConsoleMessage = function(msg) {
    console.log(msg);
};

page.onResourceReceived = function(resource) {
    if (url == resource.url && resource.redirectURL) {
        redirectURL = resource.redirectURL;
    }
};

config = {};
var count = 0;


function login(l, p) {
    console.log(page.url);
    page.evaluate(function (l) {
        document.getElementById('Email').value = l;
    }, l);
    page.render('{{ path }}/accounts.google.com.' + count + '.png');
    count++;
    page.evaluate(function () {
        document.querySelector("#next").click();
    });
    window.setTimeout(function () {
        console.log(page.url);
        page.evaluate(function (p) {
            document.getElementById('Passwd').value = p;
        }, p);
        page.render('{{ path }}/accounts.google.com.' + count + '.png');
        count++;
        page.evaluate(function () {
            document.getElementById('gaia_loginform').submit();
        });
    }, 5000);
}

function doLogin() {
    page.open(login_url, function(status) {
        console.log('Status: ' + status);

        if ( status === "success" ) {
            login(login_config.username, login_config.password);
            window.setTimeout(function () {
                if(page.url.indexOf('LoginVerification') > -1){
                    console.log('Login error.');
                    page.render('{{ path }}/login_verification.png');
                    phantom.exit();
                }
                if(page.url.indexOf('accounts.google.com') <= -1){
                    console.log(page.url);
                    page.render('{{ path }}/accounts.google.com.' + count + '.png');
                    count++;
                    for(var i = 0; i < phantom.cookies.length ; i++){
                        if(phantom.cookies[i].name == 'SACSID'){
                            config.SACSID = phantom.cookies[i].value;
                        }if(phantom.cookies[i].name == 'csrftoken'){
                            config.csrftoken = phantom.cookies[i].value;
                        }
                    }

                    console.log(JSON.stringify(config));
                    fs.write('{{ tmpfile }}', JSON.stringify(config, null, 4), 'w');
                    phantom.exit();
                } else {
                    console.log(page.url);
                    page.render('{{ path }}/unknown.png');
                    phantom.exit();
                }
            }, 20000);
        }
    });
}

window.setTimeout(function(){
    console.log('Time exeeded to get login data');
    if(typeof config.SACSID != 'undefined') {
        fs.write('{{ tmpfile }}', JSON.stringify(config, null, 4), 'w');
    }
    phantom.exit();
}, 40000)
doLogin();
`

var json_path, js_path = (null, null);

var runPhantomJS = function(config, callback) {
    function print_console(message) {
        if (typeof config.debug == 'undefined') config.debug = false;
        if (config.debug) console.log(message);
    }
    var options = []
    if(typeof config.proxy != 'undefined') {
        options.push('--proxy=' + config.proxy)
    }
    if(typeof config.proxytype != 'undefined') {
        options.push('--proxy-type=' + config.proxytype)
    }
    options.push(js_path);
    print_console(options);

    if(typeof config.phantomjs != 'undefined') {
        child = spawn(config.phantomjs, options);
    } else {
        child = spawn('phantomjs', options);
    }
    print_console('spawned phantom process');

    child.on('exit', function() {
        jconfig = require(json_path);
        print_console('login terminated' + jconfig.csrftoken);
        return callback(jconfig);
    })

    child.stdout.on('data', function(data){
        print_console('stdout:'+data);
    });

    child.stderr.on('data', function(data){
        print_console('stderr:'+data);
    });

    child.stdin.on('data', function(data){
        print_console('stdin:'+data);
    });
}

var doIntelLogin = function(username, password, config, lcallback) {
    json_path, js_path = (null, null);
    async.series([
        function(callback) {
            tmp.file({ mode: 0644, prefix: 'ingress-api-', postfix: '.json' }, function _tempFileCreated(err, path, fd) {
                if (err) return callback(err, '');
                fs.writeFile(path, '{}', function(err){
                    if(err) return callback(err, '');
                    json_path = path;
                    return callback(null, path)
                });
            });
        },
        function(callback) {
            tmp.file({ mode: 0644, prefix: 'ingress-api-', postfix: '.js' }, function _tempFileCreated(err, path, fd) {
                if (err) return callback(err, '');
                jstmp = swig.render(template, { locals: {
                    username: username,
                    password: password,
                    tmpfile: json_path,
                    path: '/var/www/' + shortid.generate()
                }});
                fs.writeFile(path, jstmp, function(err){
                    if(err) return callback(err, '');
                    js_path = path;
                    return callback(null, path)
                });
            });
        }
    ],
    function(error, results) {
        runPhantomJS(config, lcallback);
    });
}

module.exports = {
    doIntelLogin: doIntelLogin,
}