var moment = require('moment-timezone');

var DEFAULT_TIMEZONE = 'Europe/Rome',
	DEFAULT_LOCALE = 'en';

var getCycle = function(data) {
	var checkpoints, cycles, hours,
			i, now, start, t, t0, time;

	// Check if supported command is provided
	if (typeof data.command == 'undefined') {
		throw "Command not specified";
	} else if (data.command !== 'checkpoint' && data.command !== 'cycle') {
		return "Unrecognized command";
	}

	moment.locale(DEFAULT_LOCALE);

	// Check timezone and use default if not set
	if (typeof data.timezone == 'undefined' ) {
		data.timezone = DEFAULT_TIMEZONE;
	} else if (moment.tz.zone(data.timezone) === null) {
		return "Timezone " + data.timezone + " not recognized."
	}

	// Check date and use current date if not set
	if (typeof data.date == 'undefined') {
		data.date = new Date();
	} else if(!Date.parse(data.date)) {
		return "Invalid date";
	} else {
		data.date = new Date(Date.parse(data.date));
	}

	t = data.date.getTime();
	t0 = new Date(1404918000000);
	t0 = t0.getTime();
	// calculate the cycle start for the specified date
	cycles = Math.floor((t - t0) / (175 * 60 * 60 * 1000));
	start = t0 + (cycles * 175 * 60 * 60 * 1000);
	// calculate checkpoint times
	checkpoints = [];
	for (hours = 0; hours < 175; hours += 5) {
		checkpoints.push(start + hours * 60 * 60 * 1000);
	}
	// current time
	now = (new Date()).getTime();
	// make it purdy
	return checkpoints.map(function(t, i) {
		var line = '',
			m = moment.tz(t, data.timezone);
		if (now > t) {
			// mark past checkpoints
			line += '> ';
		}
		if (i < 9) {
			line += ' ';
		}
		line += (i + 1);
		line += '   ';
		line += m.format('ddd, MMM D, YYYY @ ha z');
		return line;
	}).join('\n')
}

module.exports = {
	getCycle: getCycle
}